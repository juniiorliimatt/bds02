package com.devsuperior.bds02.resources;

import com.devsuperior.bds02.dto.CityDTO;
import com.devsuperior.bds02.services.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/cities")
public class CityController {

  private final CityService service;

  @Autowired
  public CityController(CityService service) {
	this.service = service;
  }

  @GetMapping
  public ResponseEntity<List<CityDTO>> findAll(Pageable pageable) {
	var pageRequest = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("name"));
	var page = service.findAll(pageRequest);
	List<CityDTO> cities = new ArrayList<>();
	page.map(cities::add);
	return ResponseEntity.ok().body(cities);
  }

  @PostMapping
  public ResponseEntity<CityDTO> insert(@RequestBody CityDTO cityDTO) {
	cityDTO = service.insert(cityDTO);
	var uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(cityDTO.getId()).toUri();
	return ResponseEntity.created(uri).body(cityDTO);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<CityDTO> delete(@PathVariable Long id) {
	service.delete(id);
	return ResponseEntity.noContent().build();
  }

}
