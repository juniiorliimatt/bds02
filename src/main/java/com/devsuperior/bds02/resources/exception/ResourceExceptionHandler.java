package com.devsuperior.bds02.resources.exception;

import com.devsuperior.bds02.services.DatabaseException;
import com.devsuperior.bds02.services.exception.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;

@ControllerAdvice
public class ResourceExceptionHandler {

  @ExceptionHandler(ResourceNotFoundException.class)
  public ResponseEntity<StandardError> entityNotFound(ResourceNotFoundException e, HttpServletRequest request) {
	var status = HttpStatus.NOT_FOUND;
	var error = new StandardError(Instant.now(), status.value(), "Resource not found", e.getMessage(),
								  request.getRequestURI());
	return ResponseEntity.status(status).body(error);
  }

  @ExceptionHandler(DatabaseException.class)
  public ResponseEntity<StandardError> databaseException(DatabaseException e, HttpServletRequest request) {
	var status = HttpStatus.BAD_REQUEST;
	var error = new StandardError(Instant.now(), status.value(), "Database exception", e.getMessage(),
								  request.getRequestURI());
	return ResponseEntity.status(status).body(error);
  }

}
